﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameDataSO))]
public class GameDataSOEditor : Editor
{
    GameDataSO gameDataSO;

    void OnEnable()
    {
        gameDataSO = (GameDataSO)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Save data as json"))
        {
            Util.FileOperations.Save(gameDataSO.pGameDataHolder, "GameData", false);
        }
    }
}
