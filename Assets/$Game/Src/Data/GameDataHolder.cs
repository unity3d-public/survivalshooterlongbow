﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Wrapper responsible for handling game data
/// </summary>
[System.Serializable]
public class GameDataHolder
{
    /// <summary>
    /// List that holds array of all Enemy's
    /// </summary>
    [SerializeField]
    EnemysStatsHolder _EnemysDataHolder = null;

    public EnemysStatsHolder pEnemysDataHolder
    { get { return _EnemysDataHolder; } }

    /// <summary>
    /// List that holds Weapons data
    /// </summary>
    [SerializeField]
    WeaponsHolder _WeaponDataHolder = null;

    public WeaponsHolder pWeaponDataHolder
    { get { return _WeaponDataHolder; } }

    /// <summary>
    /// List that holds Player data
    /// </summary>
    [SerializeField]
    PlayerData _PlayerData = null;

    public PlayerData pPlayerData
    { get { return _PlayerData; } }
}
