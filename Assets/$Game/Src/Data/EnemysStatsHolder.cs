﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemysStatsHolder
{
    [SerializeField]
    EnemyData[] _Enemy = null;

    [SerializeField]
    public EnemyData[] pEnemy
    { get { return _Enemy; } }
}
