﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weapon
{
    [System.Serializable]
    public enum WeaponType
    { NormalGun, }

    [SerializeField]
    WeaponType _WeaponType = WeaponType.NormalGun;

    public WeaponType pWeaponType
    { get { return _WeaponType; } }

    [SerializeField]
    int _DamagePerShot = 20;

    public int pDamagePerShot
    { get { return _DamagePerShot; } }

    [SerializeField]
    float _TimeBetweenBullets = 0.15f;

    public float pTimeBetweenBullets
    { get { return _TimeBetweenBullets; } }

    [SerializeField]
    int _Range = 100;

    public int pRange
    { get { return _Range; } }
}

