﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class EnemyData
{
    [System.Serializable]
    public enum EnemyType
    { Zombear, Zombunny , Hellephant }

    [SerializeField]
    EnemyType _EnemyType = EnemyType.Zombear;

    public EnemyType pEnemyType

    { get { return _EnemyType; } }

    [SerializeField]
    int _StartingHealth = 100;

    public int pStartingHealth
    { get { return _StartingHealth; } }

    [SerializeField]
    float _SinkSpeed = 6f;

    public float pSinkSpeed
    { get { return _SinkSpeed; } }

    [SerializeField]
    int _ScoreValue = 10;

    public int pScoreValue
    { get { return _ScoreValue; } }

    [SerializeField]
    float _TimeBetweenAttacks = 0.5f;

    public float pTimeBetweenAttacks
    { get { return _TimeBetweenAttacks; } }

    [SerializeField]
    float _MoveSpeed = 3f;

    public float pMoveSpeed
    { get { return _MoveSpeed; } }

    [SerializeField]
    int _AttackDamage = 10;

    public int pAttackDamage
    { get { return _AttackDamage; } }
}
