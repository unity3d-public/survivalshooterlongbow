﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerData
{
    [SerializeField]
    int _StartingHealth = 100;

    public int pStartingHealth
    { get { return _StartingHealth; } }

    [SerializeField]
    float _MoveSpeed = 6f;

    public float pMoveSpeed
    { get { return _MoveSpeed; } }

    [SerializeField]
    float _FlashSpeed = 5f;

    public float pFlashSpeed
    { get { return _FlashSpeed; } }
}
