﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeaponsHolder
{
    [SerializeField]
    Weapon[] _Weapons = null;

    [SerializeField]
    public Weapon[] pWeapons
    { get { return _Weapons; } }
}