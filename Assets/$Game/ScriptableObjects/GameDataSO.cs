﻿using UnityEngine;

/// <summary>
/// Scriptable object resposible for Holding Game Data
/// </summary>
[CreateAssetMenu(fileName = "GameData", menuName = "CreateGameData", order = 1)]
public class GameDataSO : ScriptableObject
{
    /// <summary>
    /// List that holds Game data
    /// </summary>
    [SerializeField]
    GameDataHolder _GameDataHolder = null;

    public GameDataHolder pGameDataHolder
    { get { return _GameDataHolder; } }
}
