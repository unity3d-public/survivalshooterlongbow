﻿using UnityEngine;
using System.Collections;
using System.Linq;


public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;


    Animator anim;
    GameObject player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    bool playerInRange;
    float timer;

    [SerializeField]
    GameDataSO gameDataSO = null;

    [SerializeField]
    EnemyData.EnemyType _EnemyType = EnemyData.EnemyType.Zombear;

    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent <Animator> ();
        InitializeData();
    }


    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == player)
        {
            playerInRange = true;
        }
    }


    void OnTriggerExit (Collider other)
    {
        if(other.gameObject == player)
        {
            playerInRange = false;
        }
    }


    void Update ()
    {
        timer += Time.deltaTime;

        if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.CurrentHealth > 0)
        {
            Attack ();
        }

        if(playerHealth.CurrentHealth <= 0)
        {
            anim.SetTrigger ("PlayerDead");
        }
    }


    void Attack ()
    {
        timer = 0f;

        if(playerHealth.CurrentHealth > 0)
        {
            playerHealth.TakeDamage (attackDamage);
        }
    }

    void InitializeData()
    {
        EnemyData enemyData = gameDataSO.pGameDataHolder.pEnemysDataHolder.pEnemy.First(e => e.pEnemyType == _EnemyType);
        timeBetweenAttacks = enemyData.pTimeBetweenAttacks;
        attackDamage = enemyData.pAttackDamage;
    }
}
