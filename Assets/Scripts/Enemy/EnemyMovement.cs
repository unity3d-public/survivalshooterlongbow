﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;

    [SerializeField]
    GameDataSO gameDataSO = null;

    [SerializeField]
    EnemyData.EnemyType _EnemyType = EnemyData.EnemyType.Zombear;

    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
        InitializeData();
    }


    void Update ()
    {
        if(enemyHealth.CurrentHealth > 0 && playerHealth.CurrentHealth > 0)
        {
            nav.SetDestination (player.position);
        }
        else
        {
            nav.enabled = false;
        }
    }

    void InitializeData()
    {
        EnemyData enemyData = gameDataSO.pGameDataHolder.pEnemysDataHolder.pEnemy.First(e => e.pEnemyType == _EnemyType);
        nav.speed = enemyData.pMoveSpeed;
    }
}
